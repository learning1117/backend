Project Details:

* Language used: Python
* Framework: Django REST Framework
* The application is Dockerized

Steps to run the project:

1) Clone the project respository
2) Install Docker and docker-compose
3) Go to terminal and run
		docker-compose up -d --build (it will bring the project up and running)

Structure of the project:

Project has two models
	1) Membership: Stores details of the user
	2) Invoice: Stores details of the invoices of the respective user

APIs in the project:

1) API for creating nad listing a Member:
	http://127.0.0.1:8000/member/list/

	Input: {"first_name": "raj","last_name": "reddy","gender": "Male","email": "rahul123@gmail.com","phone_number": "+35699693597","city": "fgura","country": "malta","address": "claydan","status": "Active","credist": 5,"start_date": "2022-10-22","end_date": "2022-10-24"}

	Output: Returns the created member list

2) API for Member Detail and Update:
	http://127.0.0.1:8000/member/<member-id> (http://127.0.0.1:8000/member/1)

3) API for creating an Invoice when user checke into a club
	http://127.0.0.1:8000/member/4/invoice-create

	Input: {"invoice_date": "2022-10-25","description": "test","amount": 66.0,"month": "January","state": "Paid","member": 1}

	Output: Invoice gets created for that month. If the membership is Cancelled or credits are 0 or end date has reached invoice will not get created.

4) API for Invoice List
	http://127.0.0.1:8000/member/4/invoice

5) API for Invoice detail, update and delete
	http://127.0.0.1:8000/member/4/invoice

Finally to Run the tests, please go inside the container using the command
	docker exec -it backend bash, then run python manage.py test



