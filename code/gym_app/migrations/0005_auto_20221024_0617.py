# Generated by Django 3.2.16 on 2022-10-24 06:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gym_app', '0004_membership_membership_detail'),
    ]

    operations = [
        migrations.AddField(
            model_name='invoice',
            name='month',
            field=models.CharField(default=None, max_length=30),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='membership',
            name='end_date',
            field=models.DateField(),
        ),
        migrations.AlterField(
            model_name='membership',
            name='start_date',
            field=models.DateField(),
        ),
    ]
