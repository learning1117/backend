# Generated by Django 3.2.16 on 2022-10-22 11:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gym_app', '0002_invoice'),
    ]

    operations = [
        migrations.AlterField(
            model_name='invoice',
            name='state',
            field=models.CharField(choices=[('Outstanding', 'Outstanding'), ('Paid', 'Paid'), ('Void', 'Void')], max_length=15),
        ),
    ]
