# Generated by Django 3.2.16 on 2022-10-25 06:41

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('gym_app', '0005_auto_20221024_0617'),
    ]

    operations = [
        migrations.AlterField(
            model_name='invoice',
            name='member',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='invoice', to='gym_app.membership'),
        ),
    ]
