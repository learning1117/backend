from django.contrib import admin
from gym_app.models import Membership, Invoice


admin.site.register(Membership)
admin.site.register(Invoice)
