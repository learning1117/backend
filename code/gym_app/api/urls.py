from django.urls import path, include
from gym_app.api.views import MembershipList, MembershipDetail, InvoiceCreate, InvoiceList, InvoiceDetail


urlpatterns = [
	path('list/', MembershipList.as_view(), name='list'),
	path('<int:pk>', MembershipDetail.as_view(), name='member-detail'),
	path('<int:pk>/invoice-create', InvoiceCreate.as_view(), name='invoice-create'),
	path('<int:pk>/invoice', InvoiceList.as_view(), name='invoice-list'),
	path('invoice/<int:pk>', InvoiceDetail.as_view(), name='invoice-detail'),
]