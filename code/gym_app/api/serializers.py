from rest_framework import serializers
from gym_app.models import Membership, Invoice


class MembershipSerializer(serializers.ModelSerializer):

	class Meta:
		model = Membership
		fields = "__all__"


class InvoiceSerializer(serializers.ModelSerializer):

	class Meta:
		model = Invoice
		fields = "__all__"