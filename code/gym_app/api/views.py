from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView
from rest_framework import generics
from rest_framework.exceptions import ValidationError
import datetime

from gym_app.models import Membership, Invoice
from gym_app.api.serializers import MembershipSerializer, InvoiceSerializer


class MembershipList(APIView):

	def get(self, request):
		member = Membership.objects.all()
		serializer = MembershipSerializer(member, many=True)
		return Response(serializer.data)

	def post(self, request):
		serializer = MembershipSerializer(data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data)
		else:
			return Response(serializer.errors)


class MembershipDetail(APIView):

	def get(self,request, pk):
		try:
			member = Membership.objects.get(pk=pk)
		except Membership.DoesNotExist:
			return Response({'Error': 'Record Does not exist'}, status=status.HTTP_404_NOT_FOUND)
		serializer = MembershipSerializer(member)
		return Response(serializer.data)

	def put(self, request, pk):
		member = Membership.objects.get(pk=pk)
		serializer = MembershipSerializer(member, data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data)
		else:
			return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

	def delete(self, request, pk):
		member = Membership.objects.get(pk=pk)
		member.delete()
		return Response(status=status.HTTP_204_NO_CONTENT)


class InvoiceCreate(generics.CreateAPIView):
	serializer_class = InvoiceSerializer

	def create(self, request, *args, **kwargs):
		pk = self.kwargs.get('pk')
		member = Membership.objects.get(pk=pk)
		end_date = str(member.end_date)
		date_today = (datetime.datetime.now())
		date_stripped = date_today.strftime('%Y-%m-%d')

		if member.status == 'Canceled':
			return Response(data={"Message": "User's Membership is cancelled!"})
		if member.credits == 0:
			return Response(data={"Message": "User does not have credits to checkin"})
		if end_date <= date_stripped:
			return Response(data={"Message": "Membership end date has reached!"})
		serializer = self.get_serializer(data=request.data)
		serializer.is_valid(raise_exception=True)
		self.perform_create(serializer)
		credit = member.credits - 1
		Membership.objects.filter(pk=pk).update(credits=credit)
		headers = self.get_success_headers(serializer.data)
		return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

	def perform_create(self, serializer):
		serializer.save()


class InvoiceList(generics.ListAPIView):
	serializer_class = InvoiceSerializer

	def get_queryset(self):
		pk = self.kwargs['pk']
		return Invoice.objects.filter(member=pk)


class InvoiceDetail(generics.RetrieveUpdateDestroyAPIView):
	queryset = Invoice.objects.all()
	serializer_class = InvoiceSerializer