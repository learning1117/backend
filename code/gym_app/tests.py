from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from gym_app.api import serializers
from gym_app import models


class MembershipTestCase(APITestCase):

	def setUp(self):
		self.member = models.Membership.objects.create(first_name="john", last_name="peter", gender="Male",
							email="john123@gmail.com", phone_number="35698987876", city="valleta",
							country="malta", address="cal", membership_detail="test", status="Active",
							credits="4", start_date="2022-10-22", end_date="2022-10-24")

	def test_membership_create(self):
		data = {
			"first_name": "john",
			"last_name": "peter",
			"gender": "Male",
			"email": "john@gmail.com",
			"phone_number": "+35698987876",
			"city": "fgura",
			"country": "malta",
			"address": "claydan apartments",
			"membership_detail": "test",
			"status": "Active",
			"credits": "5",
			"start_date": "2022-10-22",
			"end_date": "2022-10-24"
		}
		response = self.client.post(reverse('list'), data)
		self.assertEqual(response.status_code, status.HTTP_200_OK)

	def test_membership_list(self):
		response = self.client.get(reverse('list'))
		self.assertEqual(response.status_code, status.HTTP_200_OK)

	def test_membership_ind(self):
		response = self.client.get(reverse('member-detail', args=(self.member.id,)))
		self.assertEqual(response.status_code, status.HTTP_200_OK)


class InvoiceTeatCase(APITestCase):

	def setUp(self):
		self.member = models.Membership.objects.create(first_name="john", last_name="peter", gender="Male",
							email="john123@gmail.com", phone_number="35698987876", city="valleta",
							country="malta", address="cal", membership_detail="test", status="Active",
							credits="4", start_date="2022-10-22", end_date="2022-10-24")
		self.invoice = models.Invoice.objects.create(member=self.member, invoice_date="2022-10-23",
							description="test", amount="4000.0", month="Feb", state="Void")

	def test_invoice_create(self):
		data = {
			"member": self.member,
			"invoice_date": "2022-10-23",
			"description": "Full body workout",
			"amount": 250.0,
			"month": "Jan",
			"state": "Paid"
		}
		response = self.client.post(reverse('invoice-create', args=(self.member.id,)), data)
		self.assertEqual(response.status_code, status.HTTP_201_CREATED)

		response = self.client.post(reverse('invoice-create', args=(self.member.id,)), data)
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

	def test_invoice_update(self):
		data = {
			"member": self.member,
			"invoice_date": "2022-10-23",
			"description": "Full body workout",
			"amount": 2300.0,
			"month": "Jan",
			"state": "Paid"
		}
		response = self.client.put(reverse('invoice-detail', args=(self.invoice.id,)), data)
		self.assertEqual(response.status_code, status.HTTP_200_OK)