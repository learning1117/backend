from django.db import models
from phonenumber_field.modelfields import PhoneNumberField


class Membership(models.Model):
	first_name = models.CharField(max_length=30)
	last_name = models.CharField(max_length=30)
	GENDER_CHOICES = [('Male', 'Male'), ('Female', 'Female'),]
	gender = models.CharField(max_length=7, choices=GENDER_CHOICES)
	email = models.EmailField(unique=True)
	phone_number = PhoneNumberField(unique=True)
	city = models.CharField(max_length=50)
	country = models.CharField(max_length=50)
	address = models.TextField()
	membership_detail = models.CharField(max_length=200, null=True, blank=True)
	STATUS_CHOICES = [('Active', 'Active'), ('Canceled', 'Canceled'),]
	status = models.CharField(max_length=10, choices=STATUS_CHOICES)
	credits = models.IntegerField(default=5)
	start_date = models.DateField()
	end_date = models.DateField()

	def __str__(self):
		return self.first_name + " " + self.last_name


class Invoice(models.Model):
	member = models.ForeignKey(Membership, on_delete=models.CASCADE, related_name='invoice')
	invoice_date = models.DateField(auto_now_add=True)
	description = models.TextField()
	amount = models.FloatField()
	month = models.CharField(max_length=30)
	STATUS_CHOICES = [('Outstanding', 'Outstanding'), ('Paid', 'Paid'), ('Void', 'Void')]
	state = models.CharField(max_length=15, choices=STATUS_CHOICES)

	def __str__(self):
		return self.member.first_name + " " + self.member.last_name